#include <sys/param.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <netinet/in.h>

#include <assert.h>
#include <errno.h>
#include <libutil.h>
#include <limits.h>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include <syslog.h>
#include <unistd.h>

#include "config.h"
#include "poppwd.h"
#include "protocol.h"

static void usage(void);
static void version(void);
static void handle_sig_alarm(int unused);

/* flag set by alarm handler (used for timeouts) */
static volatile sig_atomic_t g_alarm_expired;
/* flag to indicate if we should be using PAM */
int g_use_pam = 0;
/* the name of our peer */
const char *g_peer_name = NULL;

int main(int argc, char *argv[]) {
	int c;
	struct sigaction sa;

	/* set up logging */
	openlog("poppwd", LOG_PID, SYSLOG_FACILITY);

	/* handle command line arguments */
	while ((c = getopt(argc, argv, "Pv")) != -1) {
		switch (c) {
		case 'P':
			/* use PAM rather than pw */
			g_use_pam = 1;
			break;
		case 'v':
			/* print version string and exit */
			version();
			break;
		case '?':
		default:
			usage();
			break;
		}
	}
	argc -= optind;
	argv += optind;

	/* make sure messages to/from the client aren't buffered more than
	   is desired */
	if (setlinebuf(stdout) != 0) {
		basic_err(EX_UNAVAILABLE, MSG_ERR_MISC, "setlinebuf failed: %m");
	}
	if (setlinebuf(stdin) != 0) {
		basic_err(EX_UNAVAILABLE, MSG_ERR_MISC, "setlinebuf failed: %m");
	}

	/* install our alarm handler to enable timeouts on read & write */
	sa.sa_handler = handle_sig_alarm;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;
	if (sigaction(SIGALRM, &sa, NULL) != 0) {
		basic_err(EX_OSERR, MSG_ERR_MISC, "sigaction(SIGALRM) failed: %m");
	}

	/* determine who we are talking to */
	if (! get_peer_name(stdin, &g_peer_name)) {
		g_peer_name = DEFAULT_PEERNAME;
	}

	/* do the actual eudora protocol */
	do_protocol();

	return(0);
}

/* print usage info and exit */
static void usage(void) {

	fprintf(stderr, "usage: poppwd [-Pv]\n");
	exit(EX_USAGE);
}

/* print version info and exit */
static void version(void) {

	printf("poppwd %s\n%s\n", VER_NUM, VER_COPYRIGHT);
	exit(EX_OK);
}

/* error functions common handler */
void basic_error_common(const int log_level, const enum message_name msg, const char *const format, va_list ap) {

	vsyslog(log_level, format, ap);
	(void)to_client(TIME_OUT, "%s", messages[msg]);
}

/* log an error to syslog, send an error message to the client and exit */
void basic_err(const int ev, const enum message_name msg, const char *const format, ...) {
	va_list ap;

	va_start(ap, format);
	basic_error_common(LOG_ERR, msg, format, ap);
	va_end(ap);

	exit(ev);
}

/* as above but only a warning and don't exit */
void basic_warn(const enum message_name msg, const char *const format, ...) {
	va_list ap;

	va_start(ap, format);
	basic_error_common(LOG_WARNING, msg, format, ap);
	va_end(ap);
}

void basic_to_client(const char *message) {

	if (to_client(TIME_OUT, "%s", message) < (signed int)(strlen(message) + 2)) {       
		/* client has probably died */
		basic_err(EX_IOERR, MSG_ERR_IO, "[%s] printf of %s failed: %m", g_peer_name, message);
	}
}

/* send a message to the client. terminate all lines with \r\n.
 * returns number of characters printed
 */
int to_client(unsigned int timeout, const char *const format, ...) {
	int rv;
	va_list ap;

	va_start(ap, format);

	/* setup timeout */
	g_alarm_expired = 0;
	alarm(timeout);

	/* print the message */
	rv = vprintf(format, ap);
	/* if that worked then terminate the line */
	if (rv > 0) {
		rv += printf("\r\n");
	}

	/* see if we timed out - rv may not be negative if we timed out when
		terminating the line */
	if (g_alarm_expired) {
		/* no point sending anything to the client so just log and exit */
		syslog(LOG_ERR, "[%s] write timeout", g_peer_name);
		exit(EX_IOERR);
	}

	/* cancel the timeout */
	alarm(0);

	va_end(ap);

	return(rv);
}

/* implement a read with a timeout.
 * 
 * timeout is the number of seconds to wait for input
 * buffer is a pointer to some memory where the read data can be stored
 * size is the size of the memory pointed to by buffer (and the maximum
 *	amount of data that will be read + 1)
 *
 * The timeout is implemented using alarm(3). This makes us susceptible to the
 * race condition as descriped in APUE 10.10. There is no simple way around
 * this and so weighing up the likely hood of this occuring and the severity of
 * it occuring I've decided to ignore it.
 *
 * returns 0 on error, -1 on timeout, 1 on success
 */
int from_client(unsigned int timeout, unsigned int size, char *buffer) {
	int length;

	/* fgets can only take a signed int so make sure we check our arg
	   will fit */
	if (size > INT_MAX) {
		errno = EDOM;
		return 0;
	}

	/* set up timeout */
	g_alarm_expired = 0;
	alarm(timeout);

	/* read data from client */
	while (fgets(buffer, (int)size, stdin) != buffer) {
		/* error of some sort */
		if (g_alarm_expired) {
			/* timeout */
			basic_err(EX_IOERR, MSG_ERR_TIME, "[%s] read timeout", g_peer_name);
		} else if (errno == EINTR) {
			/* non timeout interrupt so restart */
			continue;
		} else if (feof(stdin)) {
			basic_err(EX_IOERR, MSG_ERR_IO, "[%s] unexpected EOF", g_peer_name);
		} else if (ferror(stdin)) {
			basic_err(EX_IOERR, MSG_ERR_IO, "[%s] read failed: %m", g_peer_name);
		}
	}

	alarm(0);

	/* find the length of the string and make sure we read all we should
	  have */
	length = strlen(buffer);
	if (buffer[length - 1] != '\n') {
		/* we ran out of buffer space so discard whatever is still left to
		be read (so we are ready for a new command next time). we are in
		"line mode" as it were so we know there must be a \n coming (or we
		wouldn't have got this far) so just fgetc till we get there. */
		for (;;) {
			switch (fgetc(stdin)) {
				case EOF:
					if (feof(stdin)) {
						basic_err(EX_IOERR, MSG_ERR_IO, "[%s] unexpected EOF while flushing buffer", g_peer_name);
					} else if (ferror(stdin)) {
						basic_err(EX_IOERR, MSG_ERR_IO, "[%s] read failed while flushing buffer: %m", g_peer_name);
					}
					break;
				case '\n':
					/* we have flushed enough */
					errno = ENOBUFS;
					return 0;
					break;
				default:
					/* more to come */
					break;
			}
		}
	} else {
		return 1;
	}

	assert(0);
	return 0;
}

static void handle_sig_alarm(int unused __attribute__((unused))) {

	/* this space was intentionally left blank but then I put something here */
	g_alarm_expired = 1;	/* alarm_expired has file scope */

	return;
}

int get_peer_name(FILE *stream, const char **peer_name) {
	char hostname[MAXHOSTNAMELEN + 1];
	struct sockaddr_storage peer_info;
	socklen_t peer_info_size;

	/* find out about our peer */
	peer_info_size = sizeof(peer_info);
	if (getpeername(fileno(stream), (struct sockaddr *)&peer_info, &peer_info_size) != 0) {
		/* something went wrong - most likely we are being run from the
		   command line so don't have a peer */
		return 0;
	}

	/* convert its address into a hostname if the DNS is configured */
	(void)realhostname_sa(hostname, sizeof(hostname), (struct sockaddr *)&peer_info, (int)peer_info_size);
	/* make sure it's null terminated */
	hostname[sizeof(hostname) - 1] = '\0';
	/* copy it to our callers variable */
	if ((*peer_name = strdup(hostname)) == NULL) {
		return 0;
	} else {
		return 1;
	}
}
