#include <assert.h>
#include <errno.h>
#include <string.h>
#include <sysexits.h>
#include <syslog.h>

#include "config.h"
#include "password.h"
#include "poppwd.h"
#include "protocol.h"

/* the actual text sent to a client. this must be kept in sync with the
	message_name enumeration in protocol.h */
const char *const messages[] = {
	"200 hello there, please tell me who you are",
	"201 your password has been changed",
	"202 goodbye, please call again",
	"203 ",
	"204 ",
	"300 and what would your current password be?",
	"301 ok, what would you like to change your password to?",
	"302 noop ok, remaining in current state",
	"303 ",
	"304 ",
	"500 an error has occured - please try again or ask for help",
	"501 an I/O error has occured - please try again or ask for help",
	"502 your time limit has expired - please type quicker next time",
	"503 huh? I wasn't expecting that - are you doing this by hand?",
	"504 authentication failed - maybe you have your username or password wrong?",
	"505 that line is too long for me to read - I'm only a small program you know!"
};

/* the (case-insensitive) text that can be received from a client. this must
	be kept in sync with the command enumeration in protocol.h */
const char *const commands[] = {
	"user",
	"pass",
	"newpass",
	"quit",
	"noop"
};

/* the actual implementation of the protocol */
void do_protocol(void) {
	char *username,
		 *old_pass,
		 *new_pass,
		 *arg;
	enum { WANT_USER,
		   WANT_OLDPASS,
		   WANT_NEWPASS,
		   DONE } state;
	enum command cmd;

	/* set the initial state */
	state = WANT_USER;

	/* print welcome banner */
	basic_to_client(messages[MSG_WELCOME]);

	for (;;) {
		(void)lex(&cmd, &arg);
		/* actions not dependant on state */
		switch (cmd) {
			case CMD_NULL:
				/* nothing has changed from our point of view */
				continue;
				break;
			case CMD_NOOP:
				basic_to_client(messages[MSG_NOOP_OK]);
				continue;
				break;
			case CMD_UNKNOWN:
				basic_to_client(messages[MSG_ERR_PROTO]);
				continue;
				break;
			case CMD_QUIT:
				basic_to_client(messages[MSG_GOODBYE]);
				return;
				break;
			default:
				/* just ignore this as they will be handled according
				   to state later on. this entry is just here to keep
				   gcc quiet */
				break;
		}

		switch (state) {
		case WANT_USER:
			if ((cmd == CMD_USER) && ((username = arg) != NULL)) {
				basic_to_client(messages[MSG_OLDPASS]);
				state = WANT_OLDPASS;
			} else {
				basic_to_client(messages[MSG_ERR_PROTO]);
			}
			break;
		case WANT_OLDPASS:
			if ((cmd == CMD_PASS) && ((old_pass = arg) != NULL)) {
				if (authenticate(username, old_pass)) {
					syslog(LOG_INFO, "[%s] login by \"%s\"", g_peer_name, username);
					basic_to_client(messages[MSG_NEWPASS]);
					state = WANT_NEWPASS;
				} else {
					syslog(LOG_NOTICE, "[%s] failed attempt to authenticate \"%s\"", g_peer_name,  username);
					basic_to_client(messages[MSG_ERR_AUTH]);
					state = DONE;
				}
			} else {
				basic_to_client(messages[MSG_ERR_PROTO]);
			}
			break;
		case WANT_NEWPASS:
			if ((cmd == CMD_NEWPASS) && ((new_pass = arg) != NULL)) {
				if (change_pass(username, new_pass)) {
					/* worked */
					syslog(LOG_NOTICE, "[%s] password changed for \"%s\"", g_peer_name, username);
					basic_to_client(messages[MSG_SUCCESS]);
					state = DONE;
				} else {
					/* failure */
					basic_err(EX_UNAVAILABLE, MSG_ERR_MISC, "[%s] change_pass failed: %m", g_peer_name);
				}
			} else {
				basic_to_client(messages[MSG_ERR_PROTO]);
			}
			break;
		case DONE:
		default:
			basic_to_client(messages[MSG_ERR_PROTO]);
			break;
		}
	}
}

int lex(enum command *cmd, char **arg) {
	char buffer[MAX_INPUT_LENGTH + 1]; /* a place to store incoming data */
	char *argptr;
	unsigned int i;
				
	/* read line */
	if (! from_client(TIME_IN, sizeof(buffer), buffer)) {
		if (errno == ENOBUFS) {
			/* ran out of buffer space */
			basic_warn(MSG_ERR_TOOLONG, "[%s] out of buffer space reading from client", g_peer_name);
		} else {
			/* don't handle this yet */
			basic_warn(MSG_ERR_MISC, "from_client failed");
		}
		*cmd = CMD_NULL;
		*arg = NULL;
		return 1;
	}

	/* try to match a command */
	*cmd = CMD_UNKNOWN;
	for (i = 0; i < (sizeof(commands) / sizeof(const char *const)); ++i) {
		if (strncasecmp(buffer, commands[i], strlen(commands[i])) == 0) {
			/* we match the first bit */
			argptr = buffer + strlen(commands[i]);
			if (strchr("\r\n\t ", *argptr) != NULL) {
				/* followed by whitespace so we have a match */
				*cmd = i;
				break;
			}
		}
	}

	if (*cmd != CMD_UNKNOWN) {
		/* strip leading whitespace from argument */
		for (; strchr("\t ", *argptr) != NULL; ++argptr) {
			/* This space intentionally left blank */
		}
		/* strip off \r\n allowing for the \r to be missing */
		*strpbrk(buffer, "\r\n") = '\0'; /* we are guarenteed a \n by
										  * from_client */
		if (*argptr != '\0') {
			if ((*arg = strdup(argptr)) == NULL) {
				basic_err(EX_UNAVAILABLE, MSG_ERR_MISC, "strdup failed: %m");
			}
		} else {
			*arg = NULL;
		}
	}

	return(1);
}
