/* $Id: config.h,v 1.2 2006/09/24 15:13:10 andrew Exp $ */

#ifndef CONFIG_H
#define CONFIG_H

/*
 * Things you might want to change.
 */

/* path to pw */
#define PW "/usr/sbin/pw"

/* time to wait for input from client (seconds) */
#define TIME_IN	(300)

/* time to wait for output to be sent to the client (seconds) */
#define TIME_OUT (300)

/* maximum length a line of input can be */
#define MAX_INPUT_LENGTH    (255 - 1)

/* logging settings */
#define SYSLOG_FACILITY LOG_AUTH

/* smallest UID who's password can be changed (non-PAM only) */
#define MIN_UID	(1000)

/* name used for PAM and logging when getpeername(3) fails */
#define DEFAULT_PEERNAME "non networked client"

/* service name to use with PAM (if PAM is in use) */
#define POPPWD_PAM_SERVICE_NAME "pop3pw"

/* numer of time to retry a password change on receipt of PAM_TRY_AGAIN */
#define POPPWD_PAM_RETRIES 3

#endif
