#include <sys/types.h>
#include <sys/wait.h>

#include <security/pam_appl.h>
#include <security/openpam.h>

#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include <syslog.h>
#include <unistd.h>

#include "config.h"
#include "password.h"
#include "poppwd.h"

int authenticate(const char *const username, const char *const pass) {

	if (g_use_pam) {
		return (authenticate_pam(username, pass));
	} else {
		return (authenticate_system(username, pass));
	}
}

int authenticate_system(const char *const username, const char *const pass) {
	struct passwd *pw;

	if ((pw = getpwnam(username)) == NULL) {
		/* no such user */
		return(0);
	}

	/* disallow attempts to change system passwords */
	if (pw->pw_uid < (uid_t)MIN_UID) {
		/* system user */
		syslog(LOG_NOTICE, "[%s] attempt to login as system user %s", g_peer_name, username);
		return(0);
	}

	if (strcmp(crypt(pass, pw->pw_passwd), pw->pw_passwd) == 0) {
		return(1);
	} else {
		return(0);
	}
}

int authenticate_pam(const char *const username, const char *const pass) {
	struct pam_conv pc;
	pam_handle_t *ph;
	int ec,
		rv;

	/* set up variables */
	rv = 1;
	pc.conv = openpam_nullconv;
	pc.appdata_ptr = NULL;

	/* initialise the pam library */
	if ((ec = pam_start(POPPWD_PAM_SERVICE_NAME, username, &pc, &ph)) != 0) {
		syslog(LOG_ERR, "pam_start failed for user %s: %s", username, pam_strerror(ph, ec));
		return 0;
	}

	/* tell PAM what it needs to know */
	if ((ec = pam_set_item(ph, PAM_RHOST, (const void *)g_peer_name)) != 0) {
		syslog(LOG_ERR, "pam_set_item failed: %s", pam_strerror(ph, ec));
		rv = 0;
		goto authenticate_pam_exit0;
	}
	if ((ec = pam_set_item(ph, PAM_AUTHTOK, (const void *)pass)) != 0) {
		syslog(LOG_ERR, "pam_set_item failed: %s", pam_strerror(ph, ec));
		rv = 0;
		goto authenticate_pam_exit0;
	}

	/* do the authentication */
	if ((ec = pam_authenticate(ph, 0)) != 0) {
		/* failed. if it wasn't due to wrong password we want to log it */
		if (ec != PAM_AUTH_ERR) {
			syslog(LOG_ERR, "pam_authenticate failed for user %s: %s", username, pam_strerror(ph, ec));
		}
		rv = 0;
	}

authenticate_pam_exit0:
	if (pam_end(ph, ec) != 0) {
		syslog(LOG_ERR, "pam_end failed: %s", pam_strerror(ph, ec));
		rv = 0;
	}

	return rv;
}

int change_pass(const char *const username, const char *const pass) {

	if (g_use_pam) {
		return (change_pass_pam(username, pass));
	} else {
		return (change_pass_system(username, pass));
	}
}

int change_pass_system(const char *const username, const char *const pass) {
	pid_t pid;
	int fds[2],
		status;
	char fd_name[FD_MAXLENGTH + 1];

	/* grab a fd to talk to pw over */
	if (pipe(fds) != 0) {
		basic_err(EX_OSERR, MSG_ERR_MISC, "pipe failed: %m");
	}
	/* convert fd[0] to ascii */
	if (snprintf(fd_name, (size_t)(FD_MAXLENGTH + 1), "%d", fds[0]) > FD_MAXLENGTH) {
		basic_err(EX_UNAVAILABLE, MSG_ERR_MISC, "snprintf failed: %m");
	}

	/* fork so we can exec safely */
	switch (pid = fork()) {
	case -1:
		/* error */
		basic_err(EX_OSERR, MSG_ERR_MISC, "fork failed: %m");
		break;
	case 0:
		/* child */
		if (execl(PW, "pw", "usermod", username, "-h", fd_name, NULL) == -1) {
			syslog(LOG_ERR, "execl of %s of behalf of process %ld failed: %m", PW, (long)getppid());
			/* parent will tell client */
			exit(EX_CONFIG);
		}
		break;
	default:
		/* parent */
		if (write(fds[1], pass, strlen(pass)) < 1) {
			basic_err(EX_IOERR, MSG_ERR_IO, "write to pw failed: %m");
		}
		if (wait(&status) == -1) {
			basic_err(EX_OSERR, MSG_ERR_MISC, "wait failed: %m");
		} else {
			if (WIFEXITED(status)) {
				if (WEXITSTATUS(status) == EX_OK) {
					/* worked ok */
					return(1);
				} else {
					basic_err(EX_UNAVAILABLE, MSG_ERR_MISC, "pw exited with code %d", WEXITSTATUS(status));
				}
			} else if (WIFSIGNALED(status)) {
				basic_err(EX_UNAVAILABLE, MSG_ERR_MISC, "pw exited on signal %d", WTERMSIG(status));
			} else {
				basic_err(EX_UNAVAILABLE, MSG_ERR_MISC, "pw exited abnormally");
			}
		}
		break;
	}

	return(0);
}

int change_pass_pam(const char *const username, const char *const pass) {
	struct pam_conv pc;
	pam_handle_t *ph;
	int ec,
		i,
		rv;

	/* set up variables */
	rv = 0;
	pc.conv = openpam_nullconv;
	pc.appdata_ptr = NULL;

	/* initialise the pam library */
	if ((ec = pam_start(POPPWD_PAM_SERVICE_NAME, username, &pc, &ph)) != 0) {
		syslog(LOG_ERR, "pam_start failed for user %s: %s", username, pam_strerror(ph, ec));
		return 0;
	}

	/* tell PAM what it needs to know */
	if ((ec = pam_set_item(ph, PAM_RHOST, (const void *)g_peer_name)) != 0) {
		syslog(LOG_ERR, "pam_set_item failed: %s", pam_strerror(ph, ec));
		goto change_pass_pam_exit0;
	}
	if ((ec = pam_set_item(ph, PAM_AUTHTOK, (const void *)pass)) != 0) {
		syslog(LOG_ERR, "pam_set_item failed: %s", pam_strerror(ph, ec));
		goto change_pass_pam_exit0;
	}

	/* do the password change */
	for (i = 0; i < POPPWD_PAM_RETRIES; i++) {
		if ((ec = pam_chauthtok(ph, 0)) != 0) {
			if (ec != PAM_TRY_AGAIN) {
				syslog(LOG_ERR, "pam_chauthtok for user %s: %s", username, pam_strerror(ph, ec));
				goto change_pass_pam_exit0;
			}
		} else {
			rv = 1;
			break;
		}
	}

change_pass_pam_exit0:
	if (pam_end(ph, ec) != 0) {
		syslog(LOG_ERR, "pam_end failed: %s", pam_strerror(ph, ec));
		/* don't reset rv here as the password will still have been
		   changed/not changed regardelss of the failure of this */
	}

	return rv;
}
