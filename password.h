/* $Id: password.h,v 1.2 2006/09/24 15:06:32 andrew Exp $ */

#ifndef PASSWORD_H
#define PASSWORD_H

#define FD_MAXLENGTH (3)

int authenticate(const char *const username, const char *const pass);
int authenticate_system(const char *const username, const char *const pass);
int authenticate_pam(const char *const username, const char *const pass);
int change_pass(const char *const username, const char *const new_pass);
int change_pass_system(const char *const username, const char *const new_pass);
int change_pass_pam(const char *const username, const char *const new_pass);

#endif
