/* $Id: protocol.h,v 1.2 2006/09/24 15:10:54 andrew Exp $ */

#ifndef PROTOCOL_H
#define PROTOCOL_H

/* types of messages that can be sent to the client. must be kept in sync
	with messages array defined elsewhere */
enum message_name {
	MSG_WELCOME = 0,
	MSG_SUCCESS,
	MSG_GOODBYE,
	MSG_OLDPASS = 5,
	MSG_NEWPASS,
	MSG_NOOP_OK,
	MSG_ERR_MISC = 10,
	MSG_ERR_IO,
	MSG_ERR_TIME,
	MSG_ERR_PROTO,
	MSG_ERR_AUTH,
	MSG_ERR_TOOLONG
};
extern const char *const messages[];

/* types of messages that can be received from the client. must be kept in
	sync with commands array defined elsewhere*/
enum command {
	CMD_USER = 0,
	CMD_PASS,
	CMD_NEWPASS,
	CMD_QUIT,
	CMD_NOOP,
	CMD_UNKNOWN = 99,
	CMD_NULL
};
extern const char *const commands[];

void do_protocol(void);
int lex(enum command *cmd, char **arg);

#endif
