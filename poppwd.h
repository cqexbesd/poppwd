/* $Id: poppwd.h,v 1.3 2006/09/24 15:17:01 andrew Exp $ */

#ifndef POPPWD_H
#define POPPWD_H

#include <stdarg.h>
#include <stdio.h>

#include "protocol.h"

#define VER_NUM	"2.0"
#define VER_COPYRIGHT	"copyright 2001, 2006 Andrew Stevenson <andrew@ugh.net.au>, All Rights Reserved."

/* flag to indicate if we should be using PAM */
extern int g_use_pam;
/* cached peer name */
extern const char *g_peer_name;

void basic_error_common(const int log_level, const enum message_name msg, const char *const format, va_list ap);
void basic_warn(const enum message_name msg, const char *const format, ...);
void basic_err(const int ev, const enum message_name msg, const char *const format, ...);
void basic_to_client(const char *message);
int from_client(const unsigned int timeout, const unsigned int size, char *buffer);
int to_client(const unsigned int timeout, const char *const format, ...);
int get_peer_name(FILE *stream, const char **peer_name);

#endif
