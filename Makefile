# $Id: Makefile,v 1.4 2006/09/24 14:52:54 andrew Exp $

PROG=	poppwd
SRCS=	poppwd.c config.h password.c password.h poppwd.h protocol.c \
		protocol.h
LDADD=	-lcrypt -lpam -lutil
MAN8=	poppwd.8

PREFIX?=	/usr/local
BINDIR=	${PREFIX}/libexec
MANDIR=	${PREFIX}/man/man

# I mistype it so often so I may as well make -DEBUG work
.if defined(EBUG)
DEBUG= 1
.endif

.if defined(DEBUG)
CFLAGS=	-W -Wall -Wbad-function-cast -Wcast-align -Wcast-qual \
	-Wchar-subscripts -Wconversion -Wmissing-prototypes -Wnested-externs \
	-Wpointer-arith -Wredundant-decls -Wshadow -Wstrict-prototypes \
	-Wwrite-strings -g
.else
CFLAGS+= -DNDEBUG
.endif

CFLAGS+= -I${PREFIX}/include -L${PREFIX}/lib

.include <bsd.prog.mk>
